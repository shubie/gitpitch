# Flux 

An application architecture for React

---

### Flux Design

- Dispatcher: Manages Data Flow
- Stores: Handle State & Logic
- Views: Render Data via React

---

![Flux Explained](https://facebook.github.io/flux/img/flux-simple-f8-diagram-explained-1300w.png)

---
<p><span class="slide-title">Elixir</span></p>

```javascript
// Include http module.
var http = require("http");

http.createServer(function (request, response) {
  request.on("end", function () {

    response.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    response.end('Hello HTTP!');
  });

// Listen on the 8080 port.
}).listen(8080);
```
---

```elixir
# comment

fun x -> x*3 end

defmodule Resume do
  
  def slap_me do
      IO.puts "echo my name"
  end

end

```